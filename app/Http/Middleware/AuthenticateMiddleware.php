<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

///set for middleware setup
use Auth;

class AuthenticateMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {
        ///This was previous code
        ///return $next($request);

        ///changes for middleware settings
        if (Auth::check()) {
            return $next($request);
        }else{
            return redirect('/login');
        }
    }
}
