<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
//for model add
use App\Models\Category;

class CategoryController extends Controller
{
    public function index()
    {
        return view('admin.category.categoryEntry');
    }


    public function save(Request $request)
    {
        //dd($request->all());
        //eloquent orm
        $categoryEntry =  new Category();

        $categoryEntry->categoryName = $request->name;
        $categoryEntry->shortDescription = $request->shortDescription;
        $categoryEntry->publicationStatus = $request->publicationStatus;
        //$categoryEntry->categoryName = $request->name;

        $categoryEntry->save();
        return redirect('/category/entry')->with('message','Data insert successfully');
    }



    public function manage()
    {
        ///All categories get from database
        $categories =  Category::all();

        return view('admin.category.categoryManage',['category'=>$categories]);
    }



    public function edit($id)
    {
        $categoryEdit = Category::where('id',$id)->first();

        return view('admin.category.categoryEdit',['category'=>$categoryEdit]);
    }


    public function update(Request $request)
    {
        //show debug data
        //dd($request->all());

        $category = Category::find($request->categoryId);

        $category->categoryName = $request->name;
        $category->shortDescription = $request->shortDescription;
        $category->publicationStatus = $request->publicationStatus;
        $category->categoryName = $request->name;

        $category->save();

        return redirect('/category/manage')->with('message','Updated successfully');
    }


    public function delete($id)
    {
        //echo $id;
        $categoryDelete = Category::find($id);

        $categoryDelete->delete();
        return redirect('/category/manage')->with('message','Deleted successfully');
    }

}
