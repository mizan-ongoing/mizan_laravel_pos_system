@extends('admin.master')


@section('website-title')
	Category Update
@endsection


@section('content-heading')
	Category
@endsection


@section('page-heading')
	Category
@endsection


@section('page-title')
	Category Update
@endsection





@section('mainContent')



        <div class="row">
          <div class="col-lg-12">
            <section class="panel">
              <header class="panel-heading">
                Update Categoroy
              </header>
              <div class="panel-body">


                <!-- <form class="form-horizontal " method="get"> -->
                {!! Form::open(['url' => 'category/edit', 'method'=>'post', 'name'=>'editForm','class'=>'form-horizontal']) !!}
                  <div class="form-group">
                    <label class="col-sm-3 control-label">Category Name</label>
                    <div class="col-sm-9">
                      <input type="text" class="form-control" name="name" value="{{ $category->categoryName }}">
                    </div>
                  </div>

                  <div class="form-group">
                      <label class="control-label col-sm-3">Shrot Description</label>
                      <div class="col-sm-9">
                        <textarea class="form-control ckeditor"  rows="6" name="shortDescription">{{ $category->shortDescription }}</textarea>
                      </div>
                  </div>

                  <div class="form-group">
                    <label class="control-label col-lg-3" for="inputSuccess">Publication Status</label>
                    <div class="col-lg-9">
                      <select class="form-control m-bot15" name="publicationStatus">
                           <option value="1">Published</option>
                           <option value="0">Unpublished</option>

                       </select>
                    </div>
                  </div>
                  <!-- Getting id for edit -->
                  <input type="hidden" name="categoryId" value="{{ $category->id }}">

                  <br/>
                  <center><button type="submit" value="Submit" class="btn btn-primary">Submit</button></center>
                <!-- </form> -->
                {!! Form::close() !!}



              </div>

              <script type="text/javascript">
                document.forms['editForm'].elements['publicationStatus'].value='{{ $category->publicationStatus }}'
              </script>



            </section>
          </div>
        </div>






@endsection